#!/usr/bin/env bash

install_docker () {
    if [[ "$OSTYPE" == "linux-gnu"* ]]; then
        # Only support Debian/Ubuntu for now
        if docker version; then
            echo "Docker has been installed"
        else
            echo "Installing Docker ..."
            sudo apt update && sudo apt install apt-transport-https ca-certificates curl software-properties-common -y
            curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
            sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
            sudo apt update && sudo apt install docker-ce -y
        fi

        if [ ! -f /usr/local/bin/docker-compose ]; then
            sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
            sudo chmod +x /usr/local/bin/docker-compose
        fi
    elif [[ "$OSTYPE" == "darwin"* ]]; then
        if docker version; then
            echo "Docker has been installed"
        else
            echo "Shit! This part was not completed yet, I'm a Linux guy, not Mac =))"
        fi
    fi
}

up () {
    cd "$(git rev-parse --show-toplevel)"/app/local_deployment || exit 
    docker-compose up -d
}

install_docker 
up