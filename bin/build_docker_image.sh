#!/usr/bin/env bash

echo "Building new docker image ..."
IMAGE_TAG="$CI_COMMIT_REF_NAME-$(git rev-parse --short HEAD)$(if git describe  --always --dirty=-dirty | grep dirty >/dev/null; then echo "-dirty"; fi)"
cd "$(git rev-parse --show-toplevel)"/app/flask/ && docker build --no-cache -t "$DOCKER_REGISTRY"/parcel_perform_interview:"$IMAGE_TAG" .

echo "Push image to ECR"
aws ecr get-login-password --region "$AWS_REGION" | docker login --username AWS --password-stdin "$DOCKER_REGISTRY"
docker push "$DOCKER_REGISTRY"/parcel_perform_interview:"$IMAGE_TAG"
