#!/usr/bin/env bash

cd "$(git rev-parse --show-toplevel)" || exit 

while read -r line;
    do 
        (
            black "$line" || exit
        )
done <<< "$(find . -name '*.py' -type f)"

if git describe  --always --dirty=-dirty | grep "dirty"; then
    echo "Repo has new changes, please use Black to standardized your Python code!" && exit 1
else
    echo "Good job!" && exit 0
fi
