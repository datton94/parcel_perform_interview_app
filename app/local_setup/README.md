# Local Environment Setup 

- Login to AWS ECR by `aws ecr get-login-password --profile victor --region ap-southeast-1 | docker login --username AWS --password-stdin 438723512299.dkr.ecr.ap-southeast-1.amazonaws.com` command, with the profile you was provided 
- Then run `docker-compose up -d`

Note:  I'm trying write a Makefile for local setup but I'm not sure I can do it in time.
