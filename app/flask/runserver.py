from notejam import app
from flask import jsonify
from flask import render_template
import json


@app.route("/health")
def healcheck():
    return jsonify(success=True)


@app.route("/home")
def helloworld():
    return render_template("base.html")


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
