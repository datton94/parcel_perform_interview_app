# parcel_perform_interview_app

This is the App Repo 

I got some trouble with Python source code, the source code was very old and deprecated now, so it's difficult to setup packages and dependencies to make it work. Actually, it didn't work due to `pysqlite2` was gone, I can't find that package anymore. I event try to setup sqlite3 and think about use `monkey paching` trick to make it work, but I don't have time to do that, so I decide to make a route in flask app to make it show a home page and reply health check requests. 

I also don't have time to make this repo become tiny because I have 2 jobs now, I write this lines at 1:30 a.m and I'm quite tired now, I worked for main job at daylight and work for freelance job at night until 0:0 a.m, after that I spend time to do this test everyday (to 3:00 a.m everyday), I really need some sleep. 

I have sketched chart of the CI/CD flow, it's an image, a draft version (sorry I don't have time to make it more beautiful), I hope you can understand it. 

Basically, I used Gitlab CI for CI and FluxCD for CD to match GitOps pull-based strategy. 

Everytime you push a new commit, CI will build a new docker image and push it to AWS ECR, fluxCD will tracking and deploy the newest image, it's automatic totally (of course only deploy from master branch)

That's it, I'm quite tired now. I will send you App Repo and Ops Repo, if you have any questions, feel free to ask me. 

